/* As soon as the DOMContent is loaded, the dropdown/slider/nav is triggered/initialized/changed */
document.addEventListener('DOMContentLoaded', function() 
{
    var elems = document.querySelectorAll('.dropdown-trigger')
    M.Dropdown.init(elems);

    var elems = document.getElementById('slider');
    var options = {'indicators': false, 'interval': 4000};
    M.Slider.init(elems, options);

    resizeNav();

    /* OnResize make nav height bigger => or else quote slider will be under nav */
    window.addEventListener('resize', resizeNav);
});

function resizeNav()
{
    console.log("bruh")
    var navHeight = document.getElementsByClassName('nav-extended')[0].clientHeight;
    document.getElementsByClassName('navbar-fixed')[0].style.height = navHeight + "px";
}

/* Gets called when user switches value in dropdown */
function setLanguage(language)
{
    var dropdownElement = document.getElementsByClassName('dropdown-trigger btn')[0];
    dropdownElement.innerHTML = language;
}

/* These get called when the user presses either one of the two slider buttons */
function sliderNext()
{
    M.Slider.getInstance(document.getElementById('slider')).next();
}

function sliderPrev()
{
    M.Slider.getInstance(document.getElementById('slider')).prev();
}
